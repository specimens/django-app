# Django App Example
## Prerequisities
### 1. Python 3
#### installation
+ Arch based
> sudo pacman -S python3
+ Ubuntu/Debian
> sudo apt-get install python3
+ Fedora
> sudo dnf install python3
### 2. pip
#### installation
+ Arch based
> sudo pacman -S python-pip
+ Ubuntu/Debian
> sudo apt-get install python3-pip
+ Fedora
> sudo dnf install python3
### 3. virtualenv
#### installation
> pip install virtualenv
## Set Up Project
1. Clone this repository
2. Create virtual environment in root folder
> virtualenv env
3. Project structure will be like this
```
django-app
  |-env
  |-sites
```
4. Activate virtual environment
> . env/bin/activate

OPTIONAL AFTER ACTIVATING VIRTUAL ENVIRONMENT

Install pylint
> pip install pylint

5. Run server
> cd sites && python manage.py runserver
6. Open Server at ```127.0.0.1:8000```